#Requires -Version 5.1
#Requires -Modules @{ ModuleName = 'BuildHelpers'; ModuleVersion = '2.0.1' }
#Requires -Modules @{ ModuleName = 'Pester'; ModuleVersion = '5.2.0' }


$pesterConfig = [PesterConfiguration]::Default
$pesterConfig.Run.Path = "$PSScriptRoot/.."
$pesterConfig.Run.PassThru = $true
$pesterConfig.Run.SkipRemainingOnFailure = 'Run'
$pesterConfig.Output.Verbosity = 'Detailed'

$result = Invoke-Pester -Configuration $pesterConfig
exit $result.FailedCount
