FROM mcr.microsoft.com/powershell:7.4-ubuntu-22.04

RUN DEBIAN_FRONTEND=noninteractive apt-get update && apt-get install -y \
    curl \
    git \
    unzip \
    wget \
    && rm -rf /var/lib/apt/lists/*

# Scoop thinks its running on windows and in ~/user/scoop so we need
# to create a deeply nested directory structure to fool it
RUN git clone https://github.com/ScoopInstaller/Scoop.git /tmp/some/users/that/is/deep/scoop

RUN sh -c "$(wget -O - https://taskfile.dev/install.sh)" -- -d -b /usr/local/bin
