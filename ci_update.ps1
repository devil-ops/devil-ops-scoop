<#
.SYNOPSIS
    Script meant to be run in a GitLab CI job to update the Scoop bucket.
    when new packages are released.

.NOTES
    to debug locally try running the following command (windows)
    wsl docker run --rm -it -v `$PWD:/work:ro `
        -e SSH_KEY=$(glab variable get SSH_KEY) `
        -e CI_REPOSITORY_URL=git@gitlab.oit.duke.edu:devil-ops/devil-ops-scoop.git `
        -e CI_COMMIT_BRANCH=dev `
        gitlab-registry.oit.duke.edu/devil-ops/devil-ops-scoop pwsh
#>


$ErrorActionPreference = "Stop"

$env:GIT_AUTHOR_NAME = "GitLab CI Scoop Updater"
$env:GIT_AUTHOR_EMAIL = "gitlab-ci-scoop-updater@noreply.duke.edu"
$env:GIT_SSH_COMMAND = "ssh -o StrictHostKeyChecking=no"
$Repo = "git@gitlab.oit.duke.edu:devil-ops/devil-ops-scoop.git"

$TempDir = "/tmp/scoop"
$ReqEnvs = @(
    "SSH_KEY"
    # "CI_REPOSITORY_URL"
    "CI_COMMIT_BRANCH"
)

$ReqBins = @(
    "ssh-agent",
    "ssh-add",
    "git"
)

if ( Test-Path "/root" ) {
    $ScoopCheckVer = "/tmp/some/users/that/is/deep/scoop/bin/checkver.ps1"
} else {
    $ScoopCheckVer = "$HOME/scoop/apps/scoop/current/bin/checkver.ps1"
}

Write-Host "Checking for required environment variables..."
$ReqEnvs | ForEach-Object {
    if ( -not ( Test-Path env:$_ )) {
        throw "Environment variable $_ is not set"
    }
}

Write-Host "Checking for required binaries..."
$ReqBins | ForEach-Object {
    if ( -not ( Get-Command $_ -ErrorAction SilentlyContinue )) {
        throw "Required binary $_ is not found"
    }
}

if ( -not $env:SSH_AGENT_PID ) {
    Write-Host "Setting up ssh-agent..."
    $SSHAgent = ssh-agent
    $ExitCode = $LASTEXITCODE
    if ( $ExitCode -ne 0 ) {
        throw "ssh-agent failed with exit code: $ExitCode"
    }
    ($SSHAgent | Select-String '(?<=SSH_AUTH_SOCK=)(.*?)(?=;)' ).matches.value | Set-Item -Force -Path env:SSH_AUTH_SOCK
    ($SSHAgent | Select-String '(?<=SSH_AGENT_PID=)(.*?)(?=;)' ).matches.value | Set-Item -Force -Path env:SSH_AGENT_PID

} else {
    Write-Host "Using existing ssh-agent..."
}

Write-Host "Adding SSH key..."
$env:SSH_KEY | ssh-add -
$ExitCode = $LASTEXITCODE
if ( $ExitCode -ne 0 ) {
    Write-Warning "ssh-add failed with exit code: $ExitCode"
    Write-Host "Trying other methods to add SSH key..."
    if ( $env:SSH_KEY_PATH ) {
        Write-Host "Using provided CI SSH key at path: $env:SSH_KEY_PATH..."
        Write-Host "Setting permissions on SSH key file..."
        chmod 600 $env:SSH_KEY_PATH
        $ExitCode = $LASTEXITCODE
        if ( $ExitCode -ne 0 ) {
            throw "chmod failed with exit code: $ExitCode"
        }
        Write-Host "Adding SSH key from file $env:SSH_KEY_PATH..."
        ssh-add $env:SSH_KEY_PATH
        $ExitCode = $LASTEXITCODE
        if ( $ExitCode -ne 0 ) {
            throw "ssh-add failed with exit code: $ExitCode"
        }
    } else {
        if (( $env:SSH_KEY | Measure-Object ).Count -eq 1 ) {
            Write-Host "SSH_KEY provided as single line string. Splitting into multiple lines..."
            $Header, $Inner, $Footer = $env:SSH_KEY -split "- | -"
            $Inner = $Inner -replace " ", "`n"
            $NewKey = "$Header-`n$Inner`n-$Footer"
            $NewKey | ssh-add -
            $ExitCode = $LASTEXITCODE
            if ( $ExitCode -ne 0 ) {
                throw "ssh-add failed with exit code: $ExitCode"
            }
        } else {
            Write-Host "Adding SSH key..."
            $env:SSH_KEY | ssh-add -
            $ExitCode = $LASTEXITCODE
            if ( $ExitCode -ne 0 ) {
                throw "ssh-add failed with exit code: $ExitCode"
            }
        }
    }
}

Write-Host "Setting up git..."
git config --global user.name $env:GIT_AUTHOR_NAME
$ExitCode = $LASTEXITCODE
if ( $ExitCode -ne 0 ) {
    throw "git config failed with exit code: $ExitCode"
}
git config --global user.email $env:GIT_AUTHOR_EMAIL
$ExitCode = $LASTEXITCODE
if ( $ExitCode -ne 0 ) {
    throw "git config failed with exit code: $ExitCode"
}

Write-Host "Setting up git repository..."
if ( Test-Path $TempDir ) {
    Write-Host "Removing existing directory $TempDir..."
    Remove-Item -Recurse -Force $TempDir
}

Write-Host "Cloning the repository: $Repo..."
git clone $Repo $TempDir
$ExitCode = $LASTEXITCODE
if ( $ExitCode -ne 0 ) {
    throw "git clone failed with exit code: $ExitCode"
}

Write-Host "Changing directory to $TempDir"
Set-Location $TempDir

Write-Host "Getting current repo package versions..."
$PreviousAppVersions = & $ScoopCheckVer -dir $TempDir *>&1

Write-Host "Checking for updates..."
& $ScoopCheckVer -dir $TempDir -update

# Check if there's any changes to the repo
Write-Host "Checking if there's any changes to the repo..."
if ( git status --porcelain ) {

    Write-Host "Changes detected, building commit message..."
    $NewAppVersions = & $ScoopCheckVer -dir $TempDir *>&1
    $CommitMsg = @"
[Scoop Auto Update]
Date: $(Get-Date -Format "yyyy-MM-dd HH:mm:ss")

Previous App Versions:
$($PreviousAppVersions -join "`n")

New Apps Versions:
$($NewAppVersions -join "`n")

"@

    Write-Host "Staging changes..."
    git add .
    $ExitCode = $LASTEXITCODE
    if ( $ExitCode -ne 0 ) {
        throw "git add failed with exit code: $ExitCode"
    }

    Write-Host "Using commit message: $CommitMsg"
    Write-Host "Committing changes..."
    git commit -m $CommitMsg
    $ExitCode = $LASTEXITCODE
    if ( $ExitCode -ne 0 ) {
        throw "git commit failed with exit code: $ExitCode"
    }

} else {
    Write-Host "No changes to the repo. Skipping commit..."
}

if ( git rev-list "@{u}..HEAD" ) {
    Write-Host "Commits found that need to be pushed to the remote repository..."

    if ( -not $env:CI -AND ( Read-Host "Do you want to push changes to the repository? (y/n)" ) -ne "y" ) {
        Write-Host "Skipping push..."
        return
    }

    git push origin $env:CI_COMMIT_BRANCH
    $ExitCode = $LASTEXITCODE
    if ( $ExitCode -ne 0 ) {
        throw "git push failed with exit code: $ExitCode"
    }

} else {
    Write-Host "No commits found that need to be pushed to the remote repository..."
}