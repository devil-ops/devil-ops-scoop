# Devil-Ops Scoop Packages

## What is `scoop`

Scoop is a package manager for Windows which makes it trival to package, share, install, and update command line applications. It's a very popular way to install developer tools. More details can be found here: https://scoop.sh/

See the full documentation here: https://github.com/ScoopInstaller/Scoop/wiki

## How do I install `scoop`

Install scoop by running the scoop installer script **non-elevated**"

(From https://scoop.sh/)
```
> Set-ExecutionPolicy RemoteSigned -Scope CurrentUser
> irm get.scoop.sh | iex
```

(Optional) Install scoop from an **elevated** command prompt. (scoop frowns on this) https://github.com/ScoopInstaller/Install#for-admin
```
irm get.scoop.sh -outfile 'install.ps1'
.\install.ps1 -RunAsAdmin [-OtherParameters ...]
# I don't care about other parameters and want a one-line command
iex "& {$(irm get.scoop.sh)} -RunAsAdmin"
```

## What is a `scoop` manifest?

Scoop manifest are the same as packages with other package managers. The provide scoop with the instructions on how to install a piece of software. Manifests are stored in a git repsitory (called a bucket) and scoop reads these manifest to determine how to install a given piece of software.

## What is a `scoop` bucket?

A scoop bucket is the same as a 'package repository' in other package managers. A bucket consists of a git repository with a colleciton of manifest files.

## How do I install `scoop` manifests (packages)?

##### First add a scoop bucket:

To add add scoop bucket, run `scoop bucket add <bucketname> https://github.com/<username>/<bucketname>`

For example, to add this repository: `scoop bucket add devil-ops https://gitlab.oit.duke.edu/devil-ops/devil-ops-scoop`

##### Then make sure scoop knows what's in the bucket

Run `scoop update`

##### Then install the manufest from the bucket:

To install a scoop manifest from this bucket run: `scoop install <bucketname>/<manifest>`

For example, to install foo package run: `scoop install devil-ops/foo`


## How can I list all packages available in a bucket?

Scoop doesn't have a good way to do this. See: https://github.com/ScoopInstaller/Scoop/issues/4412

You can of course `scoop search | sls <bucketname>` which will only return results which have that bucket in their description but this is very slow, searches ALL buckets and you may hit github rate limits.

View availble apps by also browsing the `./bucket` directory in this repository

From your local cli, once you've done a `scoop update` you can ls on the list of packages `ls ~\scoop\buckets\devil-ops\bucket\`

## How do I install a specific version of a app?

Append `@<version>` to target a specific version. For instance `scoop install foo@1.1.0`
